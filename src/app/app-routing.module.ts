import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import { HeroComponent } from './hero.component';
import {HeroDetailComponent} from './hero-detail.component';
import {DashboardComponent} from './dashboard.component';

const routes: Routes = [
    { path: '', redirectTo:'./dashboard', pathMatch:'full' },
    { path: 'hero',  component: HeroComponent },  
    { path: 'dashboard',  component: DashboardComponent },
    { path: 'detail/:id', component: HeroDetailComponent }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})

export class AppRoutingModule{}