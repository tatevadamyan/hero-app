import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HeroService} from './hero.service';
import { AppComponent } from './app.component';
import { HeroComponent } from './hero.component';
import {HeroDetailComponent} from './hero-detail.component';
import {AppRoutingModule} from './app-routing.module';
import {DashboardComponent} from './dashboard.component';

@NgModule({
  declarations: [
    AppComponent,
    HeroDetailComponent,
    HeroComponent,
    DashboardComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule
  ],
  providers: [HeroService],
  bootstrap: [ AppComponent]
})
export class AppModule { }
